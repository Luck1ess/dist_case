import os
from typing import List

from pyspark.sql import SparkSession, DataFrame
from pyspark import SparkConf
from pyspark.sql import functions as F
from pyspark.sql.types import FloatType
from geopy.distance import geodesic


class SparkWorker:
    def __init__(self):
        self.data_path = f"/{os.getenv('WORKDIR')}/calculations/data/sample_data.json"
        conf = (
            SparkConf()
                .setAppName("Spark Worker")
                .setExecutorEnv("PYSPARK_PYTHON", "python3")
                .setExecutorEnv("PYSPARK_DRIVER_PYTHON", "python3")
        )

        self.spark = SparkSession.builder.config(conf=conf).getOrCreate()
        self.df = self.load_data()
        self.df.count()

    def load_data(self) -> DataFrame:
        return self.spark.read.json(self.data_path).cache()

    @staticmethod
    @F.udf(returnType=FloatType())
    def geodesic_udf(a, b):
        return geodesic(a, b).m

    def find_clients(self, latitude, longitude, length) -> List[str]:
        clients_rows = (
            self.df
                .withColumn("length", self.geodesic_udf("location", F.expr(f"array({latitude}, {longitude})")))
                .where(f"length <= {length}")
                .select("name")
                .collect()
        )
        clients = [row["name"] for row in clients_rows]
        return clients
