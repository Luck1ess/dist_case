from fastapi import FastAPI

from calculations.spark_worker import SparkWorker

spark_worker = SparkWorker()

app = FastAPI()


@app.get("/")
def root():
    return {"message": "Please use /find_clients/ endpoint or see /docs/ for swagger"}


@app.get("/find_clients/")
def find_clients(latitude: float, longitude: float, length: int):
    return {"clients": spark_worker.find_clients(latitude, longitude, length)}
