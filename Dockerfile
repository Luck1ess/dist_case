FROM python:3
COPY --from=openjdk:8-jre-slim /usr/local/openjdk-8 /usr/local/openjdk-8

ENV WORKDIR=dist_case
ENV PYTHONPATH=PYTHONPATH:${WORKDIR}

WORKDIR /${WORKDIR}

COPY . .

EXPOSE 80

RUN  apt-get update  -y \
      && pip install --no-cache-dir -r requirements.txt \
      && apt-get clean

ENV JAVA_HOME /usr/local/openjdk-8