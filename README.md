To test application you need to have docker on your pc

After cloning from gitlab you need to run 'docker-compose up'
You will have http://0.0.0.0/find_clients/ endpoint with query parameters: 
1) latitude - float
2) longitude - float
3) length - integer, meters

Look at http://0.0.0.0/docs to use swagger